/* eslint-env jest */
import { visit } from 'unist-util-visit'

import mod, { SHORT, bcvParser, bookRange, prepareEntry } from '.'

const POS = {
  start: {
    line: 1
  }
}

const NODE = {
  attributes: {},
  position: POS
}

const OPTS = {
  defaultIndexKey: 'S'
}

const VERSES = {
  '': {
    input: ''
  },
  '-': {
    input: ' '
  },
  '.': {
    input: '.'
  },
  '..': {
    input: '..'
  },
  a: {
    input: 'Gen 1:1',
    short: 'Gen. 1:1'
  },
  b: {
    input: 'Garbage 1:1'
  },
  c: {
    input: 'Gen 1:1a',
    short: 'Gen. 1:1a'
  },
  d: {
    input: 'Genesis',
    short: 'Gen.'
  },
  e: {
    input: 'Rev 1:1',
    short: 'Rev. 1:1'
  },
  f: {
    input: 'Gen 1:1; Rev 1:1',
    short: 'Gen. 1:1; Rev. 1:1'
  }
}

const genEntities = key => bcvParser
  .parse(VERSES[key].input)
  .parsed_entities()
  .map(e => e.entities)
  .flat()

const genEntries = key => genEntities(key)
  .map(e => prepareEntry(NODE, e, OPTS))

function gennode (n) {
  const m = n.match(/^(.*?):(.*?)(?::(.*))?$/)
  switch (m[1]) {
    case 'vx':
      return {
        type: 'text-vx',
        children: [{
          type: 'text',
          value: VERSES[m[2]].input,
          position: POS
        }],
        attributes: {
          k: m[3]
        },
        position: POS
      }
    case 'v':
      return {
        type: 'text-v',
        children: [{
          type: 'text',
          value: VERSES[m[2]].input,
          position: POS
        }],
        attributes: {},
        position: POS
      }
    case 'vr':
      return {
        type: 'verseref',
        children: [{
          type: 'text',
          value: VERSES[m[2]].input,
          position: POS
        }],
        entities: genEntities(m[2]),
        attributes: {},
        position: POS,
        value: VERSES[m[2]].short,
        text: VERSES[m[2]].input
      }
    case 'vq':
      return {
        type: 'text-vq'
      }
    case 't':
      return {
        type: 'text',
        value: m[2],
        position: POS
      }
    case 'gs':
      return {
        type: 'indexgroupstart',
        key: `S:${m[3]}`,
        entries: [...genEntries(m[2])],
        position: POS
      }
    case 'ge':
      return {
        type: 'indexgroupend',
        entries: [...genEntries(m[2])],
        key: `S:${m[3]}`,
        position: POS
      }
  }
  throw new Error(`unknown node type ${m[1]}`)
}

function cleandoc (root) {
  let types = ['indexgroupstart', 'indexgroupend']
  visit(root, types, (node, index, parent) => {
    delete node.entities
    delete node.children
    delete node.text
    delete node.shortcut
    delete node.attributes
    delete node.count
  })

  types = ['indexgroupstart', 'verseref']
  visit(root, types, (node, index, parent) => {
    delete node.osis
  })
}

function gendoc (spec) {
  return {
    type: 'root',
    children: spec
      .split(/ /g)
      .map(gennode)
  }
}

describe('bookRange', () => {
  [
    ['Gen 1:1', ['Gen.', '1:1']],
    ['Gen 1:1-5', ['Gen.', '1:1–5']],
    ['Gen 1:1-2:5', ['Gen.', '1:1–2:5']]
  ]
    .forEach(([i, e]) => {
      it(i, () => {
        const entities = bcvParser.parse(i)
          .parsed_entities()
          .map(e => e.entities)
          .flat()
        expect(entities.length).toEqual(1)
        expect(bookRange(entities[0], SHORT)).toEqual(e)
      })
    })
})

describe('verse values', () => {
  const m = mod();
  [
    ['v', 'Gen 1:1', 'Gen. 1:1'],
    ['v', 'Gen 1:1-2', 'Gen. 1:1–2'],
    ['v', 'Gen 1:1-2:1', 'Gen. 1:1–2:1'],
    ['v', 'Gen 1:1a', 'Gen. 1:1a'],
    ['v', 'Gen 2:1a', 'Gen. 2:1a'],
    ['v', 'Gen 1:1-2a', 'Gen. 1:1–2a'],
    ['v', 'Gen 1:1b-2', 'Gen. 1:1b–2'],
    ['v', 'Gen 1:1-2:1a', 'Gen. 1:1–2:1a'],
    ['v', 'Gen 1:1a-2:1a', 'Gen. 1:1a–2:1a'],
    ['vl', 'Gen 1:1', 'Genesis 1:1'],
    ['v', '3Jo 1:6b-8', '3 John 6b–8']
  ]
    .forEach(([t, i, e]) => {
      it(i, () => {
        const f = { messages: [] }
        f.message = (x, y, z) => d.messages.push(z.replace(/.*:/, ''))
        const d = {
          type: 'root',
          children: [{
            type: `text-${t}`,
            children: [{
              type: 'text',
              value: i
            }]
          }]
        }
        m(d, f)
        expect(d.children.length).toEqual(1)
        expect(d.children[0].value).toEqual(e)
        expect(f.messages).toEqual([])
      })
    })
})

describe('index', () => {
  const m = mod();
  [
    ['vx:. v:a vx:', 'gs:a vr:a ge:a'],
    ['vx:. v:a v:e vx:', 'gs:a vr:a vr:e ge:a'],
    ['vx:.. v:a v:e vx:', 'gs:f vr:a vr:e ge:f'],
    ['vx:a vx:', 'gs:a ge:a'],
    ['vx:f vx:', 'gs:f ge:f'],
    ['vx:. v:a vx: vx:. v:e vx:', 'gs:a vr:a ge:a gs:e vr:e ge:e'],
    ['vx:a:a vx:e:b vx::a vx::b', 'gs:a:a gs:e:b ge:a:a ge:e:b']
  ]
    .forEach(([i, e]) => {
      it(`${i} --- ${e}`, () => {
        const f = { messages: [] }
        f.message = (x, y, z) => f.messages.push(z.replace(/.*:/, ''))
        const d = gendoc(i)
        m(d, f)
        cleandoc(d)
        expect(f.messages).toEqual([])
        expect(d).toEqual(gendoc(e))
      })
    })
})

describe('messages', () => {
  const f = mod();
  [
    ['v:b', 'parseable-refs'],
    ['vx:. vx:', 'shortcut-fulfilled'],
    ['vx:.. v:a vx:', 'shortcut-fulfilled'],
    ['vx:. v:d vx:', 'no-book-index'],
    ['vx:a vx:a v:d vx:', 'no-double-starts'],
    ['vx:', 'index-started'],
    ['vx:a', 'index-ended']
  ]
    .forEach(([i, e]) => {
      it(e, () => {
        const d = { messages: [] }
        d.message = (x, y, z) => d.messages.push(z.replace(/.*:/, ''))
        const r = gendoc(i)
        f(r, d)
        expect(d.messages).toEqual([e])
      })
    })
})
