import { toText } from '@inkylabs/remark-utils'
import { visit } from 'unist-util-visit'
import refparser from 'bible-passage-reference-parser/js/en_bcv_parser.js'
import osisToEn from 'bible-reference-formatter'

const BcvParser = refparser.bcv_parser
const bcvParser = new BcvParser()
bcvParser.set_options({
  book_alone_strategy: 'full',
  book_sequence_strategy: 'include'
})
const bookOrder = bcvParser.translations.default.order

const NAME = 'verses'

// TODO: Make this an option.
const LONG = 'esv-long'
const SHORT = 'esv-short'

const bookSpaceRe = new RegExp('(?<=\\b[1-3])[ \\n](?=' + [
  'Samuel',
  'Kings',
  'Chronicles',
  'Corinthians',
  'Thessalonians',
  'Timothy',
  'Peter',
  'John'
]
  .join('|') + ')\\b', 'g')

const singleChapterBooks = [
  'Obadiah',
  '2 John',
  '3 John',
  'Jude'
]

// TODO: Write a test to ensure these all match up with what Carissa sent.
// TODO: Write a test to make sure all our 3 name books work (including Php).
// TODO: Write a test to ensure 3 John 8 shows up correctly.
// TODO: Write a test for partial verses.
// TODO: Error if we see any outside text strings that contain a verse?
function format (osis, style) {
  let en
  try {
    en = osisToEn(style, osis)
  } catch (e) {
    console.error(`Could not format ${osis}`)
  }
  return en
    // Numbered books should have non-breaking spaces.
    .replace(/(?<=[12]) (?=[A-Z])/g, '\u00A0')
    // Ranges should be en-dashes, not em-dashes.
    .replace(/—/g, '–')
}

// Splits the book from the chapter/verse range.
// Example: 'Gen 1:1-2:1' -> ['Gen.', '1:1-2:1']
function bookRange (entity, style) {
  const formatted = format(entity.osis, style)
  const m = formatted.match(/(.*) (\d.*)/)
  if (m) {
    return [m[1], m[2]]
  }
  return [formatted, '']
}

function prepareEntry (node, entity, opts) {
  // We don't need to include partial verse info in the index.
  const [b, r] = bookRange(entity, LONG)
  return {
    indexKey: node.attributes.ik || opts.defaultIndexKey,
    entryKey: [
      entity.start.c,
      entity.start.v,
      entity.end.c,
      entity.end.v
    ]
      .map(v => String(v).padStart(3, '0'))
      // I'm not sure what the space is needed for, but it won't sort
      // correctly without it in LaTeX.
      .join('') + ' ',
    entryName: {
      type: 'text',
      value: r,
      // TODO: This could be more precise.
      position: node.position
    },
    groupKey: String(bookOrder[entity.start.b]).padStart(2, '0'),
    groupName: b,
    bookOnly: !r
  }
}

// Note that adjacent verses will be combined. (e.g., 1Ti 1:1,2 -> 1Ti 1:1-2)
export default (opts) => {
  opts = Object.assign({
    defaultIndexKey: 'S'
  }, opts)

  return (root, f) => {
    // Get text of all nodes.
    visit(root, ['text-vx', 'text-v', 'text-vl'], (node, index, parent) => {
      node.text = toText(node)
    })

    // Distinguish start and end nodes.
    visit(root, 'text-vx', (node, index, parent) => {
      node.type = node.text ? 'text-vx-start' : 'text-vx-end'
      node.entries = []
      const m = node.text.match(/^\.*$/)
      if (m) {
        node.shortcut = true
        node.count = m[0].length
      }
    })

    // Parse entities.
    let types = ['text-vx-start', 'text-v', 'text-vl']
    visit(root, types, (node, index, parent) => {
      node.entities = []
      if (node.shortcut) return
      const bcvRef = bcvParser.parse(node.text)
      if (!bcvRef.entities.length) {
        f.message(`Could not parse ref: "${node.text}"`, node.position,
          `${NAME}:parseable-refs`)
        node.value = node.text
        return
      }
      node.osis = bcvRef.osis()
      node.entities = bcvRef
        .parsed_entities()
        .map(e => e.entities)
        .flat()
    })

    // Format verses for display.
    visit(root, ['text-v', 'text-vl'], (node, index, parent) => {
      if (!node.osis) return
      const style = node.type === 'text-v' ? SHORT : LONG
      node.type = 'verseref'
      node.value = format(node.osis, style)
      for (const e of node.entities) {
        // Add partial markers back on to verses.  E.g., Gen 1:1a.
        const blong = bookRange(e, LONG)[0]
        const b = bookRange(e, style)[0]
        let p = node.value.indexOf(b)
        if (p < 0) {
          f.message(`Could not find book "${b}" in "${node.value}", ` +
            `parsed from "${node.text}"`, node.position,
            `${NAME}:partial-book-findable`)
          return
        }
        // Grab the segments of verses that have partials and reattach the
        // partial to each in the formatted versions.
        for (const m of node.text.matchAll(/((?:\d+:|-)\d+)([a-g])/g)) {
          let v = m[1].replace(/-/, '–')
          if (singleChapterBooks.includes(blong)) {
            v = v.replace('1:', '')
          }
          p = node.value.indexOf(v, p + 1)
          if (p < 0) {
            f.message(`Could not find range number "${m[1]}" in ``"${node.value}" after ${p}, parsed from "${node.text}"`,
              node.position, `${NAME}:partial-range-findable`)
            continue
          }
          p += v.length
          node.value = node.value.slice(0, p) + m[2] + node.value.slice(p)
        }
      }
    })

    // Populate shortcut entities.
    let dotnode = null
    types = ['text-vx-start', 'text-vx-end', 'text-vq', 'verseref']
    visit(root, types, (node, index, parent) => {
      switch (node.type) {
        case 'verseref':
          if (!dotnode) return
          dotnode.entities.push(...node.entities)
          dotnode.count--
          if (!dotnode.count) dotnode = null
          break
        case 'text-vx-start':
          if (node.shortcut) dotnode = node
          break
        case 'text-vx-end':
          dotnode = null
          break
      }
    })

    // Find start nodes without references.
    visit(root, 'text-vx-start', (node, index, parent) => {
      if (!node.count) return
      f.message('Could not find enough references for shortcut verse index',
        node.position, `${NAME}:shortcut-fulfilled`)
    })

    // Populate start entries.
    visit(root, 'text-vx-start', (node, index, parent) => {
      node.entries.push(...node.entities
        .map(e => {
          // We don't need to include partial verse info in the index.
          const entry = prepareEntry(node, e, opts)
          if (entry.bookOnly) {
            f.message('cannot use book-only citation in Scripture index',
              node.position, `${NAME}:no-book-index`)
          }
          return entry
        }))
    })

    // Match start with end.
    const starts = {}
    visit(root, ['text-vx-start', 'text-vx-end'], (node, index, parent) => {
      const indexKey = node.attributes.ik || opts.defaultIndexKey
      node.key = `${indexKey}:${node.attributes.k}`
      if (node.type === 'text-vx-start') {
        if (starts[node.key]) {
          f.message(`already have index started for ${node.key}`,
            node.position, `${NAME}:no-double-starts`)
        }
        node.type = 'indexgroupstart'
        starts[node.key] = node
        return
      }

      const start = starts[node.key]
      if (!start) {
        f.message(`cannot find start for ${node.key}`,
          node.position, `${NAME}:index-started`)
        return
      }
      node.type = 'indexgroupend'
      node.entries = start.entries
      delete starts[node.key]
    })

    // Find unended nodes.
    Object.values(starts)
      .forEach(s => {
        f.message(`cannot find end for ${s.key}`,
          s.position, `${NAME}:index-ended`)
      })

    // Stitch known book names together.
    visit(root, 'text', (node, index, parent) => {
      node.value = node.value.replace(bookSpaceRe, '\u00A0')
    })
  }
}

export {
  SHORT,
  bcvParser,
  bookRange,
  prepareEntry
}
